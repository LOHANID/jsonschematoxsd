package com.openapi.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class Utils {
    public static String normalize(String name) {
        return name.replaceAll("[,!\\\"#$%&'()*+/:;<=>?@^`{|}~]+","_");
    }

    public static boolean isNull(String s) {
        return s == null ? true : s.isEmpty();
    }

    public static String getStackTrace(Throwable e) {
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw, true);
        e.printStackTrace(pw);
        return sw.getBuffer().toString();
    }
}
