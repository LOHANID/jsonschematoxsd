package com.openapi.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.openapi.constants.JSONField;
import com.openapi.constants.JSONType;
import com.openapi.constants.XSDType;
import io.swagger.v3.core.util.Json;
import org.springframework.util.Assert;


public class TypeResolver {

    private final JsonNode node;
    private final String type;

    public TypeResolver(JsonNode node) {
        this.node = node;
        this.type = parseType();
    }

    public String type() {
        return this.type;
    }

    public XSDType xsdType() {
        if (isRef()) return XSDType.REFERENCE;
        if (isAnyOf()) return XSDType.ANYOF;
        if (isArray()) return XSDType.ARRAY;
        if (isObject()) return XSDType.OBJECT;

        Assert.notNull(type(), "type must be specified on node :" + Json.pretty(node));
        final XSDType xsdType = getTypeFromTypeMappings();
        Assert.notNull(xsdType, "Unable to determine XSD type for json type=" + type + ", format=" + format());
        return xsdType;
    }

    public JSONType jsonType() {
        if (isRef()) return JSONType.ref;
        if (isAnyOf()) return JSONType.anyOf;
        if (isAllOf()) return JSONType.allOf;
        if (isOneOf()) return JSONType.oneOf;
        if (isArray()) return JSONType.array;
        if (isObject()) return JSONType.object;
        if (type.equals("boolean")) return JSONType.bool;

        Assert.notNull(type, "type must be specified on node :" + Json.pretty(node));

        return JSONType.valueOf(type);
    }

    private boolean isOneOf() {
        return node.get(JSONType.oneOf.toString()) != null;

    }

    private boolean isAllOf() {
        return node.get(JSONType.allOf.toString()) != null;

    }

    public boolean isObject() {
        if (node.get(JSONField.type.name()) == null) {
            return false;
        }
        return JSONType.object.toString().equals(node.get(JSONField.type.name()).asText());
    }

    public boolean isArray() {
        if (node.get(JSONField.type.name()) == null) {
            return false;
        }
        return JSONType.array.toString().equals(node.get(JSONField.type.name()).asText());
    }

    /**
     * {"description":"Optional information related to the business.","nullable":true,
     * "anyOf":[{"$ref":"#/components/schemas/account_business_profile"}]}
     */
    public boolean isAnyOf() {
        return node.get(JSONType.anyOf.toString()) != null;
    }


    private XSDType getTypeFromTypeMappings() {
        final String key = (type + (format() != null ? ("|" + format()) : "")).toLowerCase();
        return TypeMappings.instance().get(key);
    }

    public boolean isRef() {
        return (node.get(JSONType.ref.toString()) != null);
    }

    public String format() {
        if (node.get(JSONField.format.name()) == null) {
            return "";
        }
        return node.get(JSONField.format.name()).textValue();
    }

    private String parseType() {
        if (!(node.get(JSONType.ref.toString()) == null)) {
            return node.get(JSONType.ref.toString()).asText().replaceAll("/", ".").replaceAll("#.", "");
        }
        if (!(node.get(JSONField.type.name())==null))
            return node.get(JSONField.type.name()).textValue();

        return JSONType.any.toString();
    }

}
