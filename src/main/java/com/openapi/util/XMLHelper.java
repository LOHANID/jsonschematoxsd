package com.openapi.util;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringWriter;
import java.util.*;


public class XMLHelper {


    private final NamespaceContext namespaceContext;
    private final Node node;
    private final XPath xPath;

    public XMLHelper(Node node, NamespaceContext namespaceContext) {
        this.namespaceContext = namespaceContext;
        this.node = node;
        this.xPath = XPathFactory.newInstance().newXPath();
        if (namespaceContext != null)
            xPath.setNamespaceContext(namespaceContext);
    }

    public XMLHelper(Node node) {
        this(node, (NamespaceContext) null);
    }

    public XMLHelper(final Node node, final Map<String, String> nsmap) {
        this(node, new NamespaceContext() {
            @Override
            public String getNamespaceURI(String prefix) {
                for (Map.Entry<String, String> entry : nsmap.entrySet()) {
                    if (entry.getKey().equals(prefix)) return entry.getValue();
                }
                return null;
            }

            @Override
            public String getPrefix(String namespaceURI) {
                for (Map.Entry<String, String> entry : nsmap.entrySet()) {
                    if (entry.getValue().equals(namespaceURI)) return entry.getKey();
                }
                return null;
            }

            @Override
            public Iterator getPrefixes(String namespaceURI) {
                for (Map.Entry<String, String> entry : nsmap.entrySet()) {
                    if (entry.getValue().equals(namespaceURI))
                        return Arrays.asList(entry.getKey().split(",")).iterator();
                }
                return null;
            }
        });
    }

    public String getTextContent(String xpath) {
        try {
            return xPath.evaluate(xpath, node);
        } catch (XPathExpressionException e) {
            return null;
        }
    }

    public Node getNode(String xpath) {
        try {
            return (Node) xPath.evaluate(xpath, node, XPathConstants.NODE);
        } catch (XPathExpressionException e) {
            return null;
        }
    }

    public NodeList getNodes(String xpath) {
        try {
            return (NodeList) xPath.evaluate(xpath, node, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            return null;
        }
    }

    public void addNode(Node node, String xpath) {
        NodeList targetNodes = getNodes(xpath);
        Document ownerDoc = null;
        for (int i = 0; i <= targetNodes.getLength(); i++) {
            if (targetNodes.item(i) == null)
                continue;
            if (targetNodes.item(i).getNodeType() != Node.ELEMENT_NODE)
                return;

            ownerDoc = targetNodes.item(i).getOwnerDocument();
            if (ownerDoc != null) {
                Node newNode = ownerDoc.importNode(node, true);
                targetNodes.item(i).appendChild(newNode);
            }
        }

    }

    public void setText(String value, String xpath) {
        NodeList targetNodes = getNodes(xpath);
        for (int i = 0; i <= targetNodes.getLength(); i++) {
            if (targetNodes.item(i) != null)
                targetNodes.item(i).setTextContent(value);
        }

    }

    public String domToString() throws Exception {
        StringWriter sw = new StringWriter();

        Transformer t = TransformerFactory.newInstance().newTransformer();
        t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        t.setOutputProperty(OutputKeys.INDENT, "yes");
        t.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        t.transform(new DOMSource(node), new StreamResult(sw));

        return sw.toString();
    }

    public void setAttribute(String name, String value) {
        Element configElement = null;
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            configElement = (Element) node;

        } else if (node.getNodeType() == Node.DOCUMENT_NODE) {
            configElement = ((Document) node).getDocumentElement();
        }
        if (configElement != null) {
            configElement.setAttribute(name, value);
        }
    }

    /*This method creates a child node with the given name under this node*/

    public Element createElement(String name, boolean createAsFirstChild) {
        final Document doc = node.getOwnerDocument() != null ? node.getOwnerDocument() : ((Document) node);
        final Element retVal = doc.createElementNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, name);
        if (doc.getDocumentElement() == null)
            doc.appendChild(retVal);
        else {
            if (createAsFirstChild)
                node.insertBefore(retVal, node.getFirstChild());
            else
                node.appendChild(retVal);
        }

        return retVal;
    }

    public Element createElement(String name) {
        return createElement(name, false);
    }

    public void validate() throws Exception {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setValidating(false);
        factory.setNamespaceAware(true);

        SAXParser parser = factory.newSAXParser();

        XMLReader reader = parser.getXMLReader();
        reader.setErrorHandler(new ErrorHandler());
        reader.parse(new InputSource(domToString()));
    }

    private class ErrorHandler implements org.xml.sax.ErrorHandler {
        private final List<SAXParseException> warnings = new LinkedList<SAXParseException>();
        private final List<SAXParseException> errors = new LinkedList<SAXParseException>();

        public void error(SAXParseException x) {
            errors.add(x);
        }

        public void fatalError(SAXParseException x) {
            errors.add(x);
        }

        public void warning(SAXParseException x) {
            warnings.add(x);
        }

        public List<SAXParseException> getErrors() {
            return errors;
        }

        public List<SAXParseException> getWarnings() {
            return warnings;
        }
    }

}
