package com.openapi.util;

import com.openapi.constants.JSONType;
import com.openapi.constants.XSDType;

import java.util.HashMap;
import java.util.Map;

public class TypeMappings {

    private static final Map<String, String> mappings = new HashMap<String, String>();


    private static final TypeMappings _this = new TypeMappings();

    static {
        _this.addTypeMapping(JSONType.string, XSDType.STRING).addTypeMapping(JSONType.object, XSDType.OBJECT)
             .addTypeMapping(JSONType.array, XSDType.ARRAY).addTypeMapping(JSONType.number, XSDType.DECIMAL)
             .addTypeMapping(JSONType.bool, XSDType.BOOLEAN).addTypeMapping(JSONType.integer, XSDType.INT)
             .addTypeMapping("int", XSDType.INT).addTypeMapping("date-time", XSDType.DATETIME)
             .addTypeMapping("time", XSDType.TIME).addTypeMapping("date", XSDType.DATE)
             .addTypeMapping(JSONType.string, "uri", XSDType.STRING)
             .addTypeMapping(JSONType.string, "email", XSDType.STRING)
             .addTypeMapping(JSONType.string, "phone", XSDType.STRING)
             .addTypeMapping(JSONType.string, "date-time", XSDType.DATETIME)
             .addTypeMapping(JSONType.string, "date", XSDType.DATE)
             .addTypeMapping(JSONType.string, "time", XSDType.TIME)
             .addTypeMapping(JSONType.string, "utc-millisec", XSDType.LONG)
             .addTypeMapping(JSONType.string, "regex", XSDType.STRING)
             .addTypeMapping(JSONType.string, "color", XSDType.STRING)
             .addTypeMapping(JSONType.string, "style", XSDType.STRING);
    }


    public TypeMappings addTypeMapping(JSONType jType, XSDType xType) {
        mappings.put(jType.toString(), xType.toString());
        return _this;
    }

    public TypeMappings addTypeMapping(String nonStandardType, XSDType xType) {
        mappings.put(nonStandardType, xType.toString());
        return _this;
    }

    public TypeMappings addTypeMapping(JSONType jType, String jsonTypeFormat, XSDType xType) {
        mappings.put(jType.toString() + "|" + jsonTypeFormat, xType.toString());
        return _this;
    }

    public static TypeMappings instance() {
        return _this;
    }

    public XSDType get(String key) {
        String type = mappings.get(key) != null ? mappings.get(key) : mappings.get(key.split("\\|")[0]);
        return XSDType.valueOf(type.toUpperCase()) != null ? XSDType.valueOf(type.toUpperCase()) : XSDType.STRING;
    }

}