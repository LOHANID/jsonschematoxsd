package com.openapi.util;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.*;

/**
 * Created by dlohani on 6/5/2017.
 */
public abstract class Logger {


    public static final String PATHNAME = "C:\\Users\\dlohani\\workspace\\ML\\logs";
    private static Handler consoleHandler = new ConsoleHandler();
    private static Handler fileHandler;

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format", "%1$tF %1$tT %4$s %2$s %5$s%6$s%n");

        try {
            fileHandler = new FileHandler(new File(PATHNAME).toString(), true);
            fileHandler.setFormatter(formatter());
            consoleHandler.setFormatter(formatter());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static SimpleFormatter formatter() {
        return new SimpleFormatter() {
            @Override
            public synchronized String format(LogRecord record) {
                return "Thread : " + record.getThreadID() + " : " + record.getLevel() + "  :  "
                        + record.getSourceClassName() + " : "
                        + record.getSourceMethodName() + " : "
                        + record.getMillis() + " : "
                        + record.getMessage() + "\n";
            }
        };
    }

    public static <T> java.util.logging.Logger get(T t) {

        java.util.logging.Logger logger  = java.util.logging.Logger.getLogger(t.getClass().getName());
        if(!Arrays.asList(logger.getHandlers()).isEmpty())
            return logger;
        logger.addHandler(consoleHandler);
        logger.addHandler(fileHandler);
        logger.setUseParentHandlers(false);
        logger.setLevel(Level.FINE);

        return logger;
    }

    public static <T> java.util.logging.Logger get() {
        java.util.logging.Logger logger = java.util.logging.Logger.getLogger("");
        return get(logger);
    }

    private static java.util.logging.Logger get(java.util.logging.Logger logger) {
        if(!Arrays.asList(logger.getHandlers()).isEmpty())
            return logger;
        logger.addHandler(consoleHandler);
        logger.addHandler(fileHandler);
        logger.setUseParentHandlers(false);
        logger.setLevel(Level.FINE);
        return logger;
    }
}
