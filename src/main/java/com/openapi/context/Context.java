package com.openapi.context;

import com.openapi.config.Config;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.servers.Server;

import java.util.List;


/**
 * This com.openapi.dto.Context class encapsulates information about the open API and the associated cloud application model.
 */
public class Context {

    private final Config config;
    private final OpenAPI api;
    private String cloudObjectNamespace = "http://xmlns.projectx.com/openapi/ns";

    public Context(OpenAPI api, Config config) {
        this.api = api;
        this.config = config;
    }

    public String openApiHostUrl() {
        return null;
    }

    public String cloudObjectNamespace() {
        return cloudObjectNamespace;
    }

    public void cloudObjectNamespace(String cloudObjectNamespace) {
        this.cloudObjectNamespace = cloudObjectNamespace;
    }


    public List<Server> getServers() {
        return api.getServers();
    }


    public String targetNS() {
        return config.targetNS();
    }

    public Config config() {
        return config;
    }

}
