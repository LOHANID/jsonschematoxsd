package com.openapi.parser;

import com.openapi.util.Utils;
import io.swagger.v3.core.util.Json;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.media.Content;
import io.swagger.v3.oas.models.media.MediaType;
import io.swagger.v3.oas.models.media.ObjectSchema;
import io.swagger.v3.oas.models.responses.ApiResponse;
import io.swagger.v3.oas.models.responses.ApiResponses;

import java.util.HashMap;
import java.util.Map;


public class OpenAPIPath {
    private final PathItem path;
    private final String resource;

    public OpenAPIPath(PathItem path, String resource) {
        this.path = path;
        this.resource = resource;
    }


    public Map<String, String> inlineRequestSchemas() {

        Map<String, String> schemas = new HashMap<String, String>();

        if (path.getGet() != null && path.getGet().getRequestBody() != null) {
            schemas.putAll(inlinedRequestSchemas(path.getGet(), "/get/"));
        }
        if (path.getPost() != null && path.getPost().getRequestBody() != null) {
            schemas.putAll(inlinedRequestSchemas(path.getPost(), "/post/"));
        }
        if (path.getPut() != null && path.getPut().getRequestBody() != null) {
            schemas.putAll(inlinedRequestSchemas(path.getPut(), "/put/"));
        }
        if (path.getTrace() != null && path.getTrace().getRequestBody() != null) {
            schemas.putAll(inlinedRequestSchemas(path.getTrace(), "/trace/"));
        }
        if (path.getDelete() != null && path.getDelete().getRequestBody() != null) {
            schemas.putAll(inlinedRequestSchemas(path.getDelete(), "/delete/"));
        }
        if (path.getPatch() != null && path.getPatch().getRequestBody() != null) {
            schemas.putAll(inlinedRequestSchemas(path.getPatch(), "/patch/"));
        }
        if (path.getHead() != null && path.getHead().getRequestBody() != null) {
            schemas.putAll(inlinedRequestSchemas(path.getHead(), "/head/"));
        }

        return schemas;
    }

    private Map<String, String> inlinedRequestSchemas(Operation operation, String suffix) {
        Map<String, String> schemas = new HashMap<String, String>();
        Content content = operation.getRequestBody().getContent();
        for (Map.Entry<String, MediaType> mediaType : content.entrySet()) {
            if (mediaType.getValue().getSchema() instanceof ObjectSchema)
                schemas.put(normalize(resource + suffix + "/request/" + mediaType.getKey()),
                        Json.pretty(mediaType.getValue().getSchema()));
        }
        return schemas;
    }

    private String normalize(String input) {
        input = Utils.normalize(input);
        return input.startsWith(".") ? input.substring(1) : input;
    }

    public Map<String, String> inlineResponseSchemas() {

        Map<String, String> schemas = new HashMap<String, String>();

        if (path.getGet() != null) {
            schemas.putAll(inlinedResponseSchemas(path.getGet(), "/get"));
        }

        if (path.getPost() != null) {
            schemas.putAll(inlinedResponseSchemas(path.getPost(), "/post"));
        }

        if (path.getPut() != null) {
            schemas.putAll(inlinedResponseSchemas(path.getPut(), "/put"));
        }

        if (path.getDelete() != null) {
            schemas.putAll(inlinedResponseSchemas(path.getDelete(), "/delete"));
        }

        if (path.getTrace() != null) {
            schemas.putAll(inlinedResponseSchemas(path.getTrace(), "/trace"));
        }

        if (path.getHead() != null) {
            schemas.putAll(inlinedResponseSchemas(path.getHead(), "/head"));
        }

        if (path.getPatch() != null) {
            schemas.putAll(inlinedResponseSchemas(path.getPatch(), "/patch"));
        }
        return schemas;
    }

    private Map<String, String> inlinedResponseSchemas(Operation operation, String suffix) {
        Map<String, String> schemas = new HashMap<String, String>();
        ApiResponses responses = operation.getResponses();
        if (responses == null || responses.isEmpty()) return schemas;
        for (Map.Entry<String, ApiResponse> response : responses.entrySet()) {
            if (response.getValue() == null || response.getValue().getContent() == null) continue;
            for (Map.Entry<String, MediaType> mediaType : response.getValue().getContent().entrySet()) {
                if (mediaType.getValue() == null || mediaType.getValue().getSchema() == null) continue;
                if (mediaType.getValue().getSchema() instanceof ObjectSchema) {
                    schemas.put(normalize(
                            resource + suffix + "/" + response.getKey() + "/response/" + mediaType.getKey()),
                            Json.pretty(mediaType.getValue().getSchema()));
                }
            }
        }
        return schemas;
    }
}
