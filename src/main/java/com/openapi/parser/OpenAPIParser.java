package com.openapi.parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openapi.config.Config;
import com.openapi.schema.JsonSchema;
import com.openapi.util.Utils;
import io.swagger.v3.core.util.Json;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.Paths;
import io.swagger.v3.parser.OpenAPIV3Parser;
import io.swagger.v3.parser.converter.SwaggerConverter;
import io.swagger.v3.parser.core.models.ParseOptions;
import io.swagger.v3.parser.core.models.SwaggerParseResult;
import org.springframework.util.Assert;
import org.w3c.dom.Document;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class OpenAPIParser {

    private final Logger logger = com.openapi.util.Logger.get();


    private final OpenAPI openAPI;

    public OpenAPIParser(OpenAPI openAPI) {
        this.openAPI = openAPI;
    }

    public OpenAPIParser(String location, boolean isSwagger) {
        if (isSwagger) this.openAPI = readLocationAndConvert(location);
        else this.openAPI = new OpenAPIV3Parser().read(location);
    }

    public OpenAPIParser(InputStream is, boolean isSwagger) throws Exception {
        if (isSwagger) this.openAPI = readContentAndConvert(read(is));
        else {
            SwaggerParseResult result = new OpenAPIV3Parser().readContents(read(is));
            validateResult(result);
            this.openAPI = result.getOpenAPI();
        }
    }

    private OpenAPI readContentAndConvert(String content) throws Exception {
        ParseOptions options = new ParseOptions();
        options.setResolveFully(false);
        SwaggerParseResult result = new SwaggerConverter().readContents(content, null, options);
        validateResult(result);
        return result.getOpenAPI();
    }

    private void validateResult(SwaggerParseResult result) throws Exception {
        StringBuilder msgs = new StringBuilder();
        for (String msg : result.getMessages()) {
            msgs.append(msg).append("/n");
        }
        logger.info(msgs.toString());
        if (result.getOpenAPI() == null) throw new Exception("Failed to create an Open API model:" + msgs.toString());
    }

    private String read(InputStream is) throws IOException {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = is.read(buffer)) != -1) {
            result.write(buffer, 0, length);
        }
        is.close();
        return result.toString();
    }

    /**
     * This method reads a {@link io.swagger.models.Swagger} from the given location and returns an {@link OpenAPI}
     * Model
     *
     * @param swaggerLocation : Location to read the swagger from
     * @return an OpenAPI Model
     */
    private OpenAPI readLocationAndConvert(String swaggerLocation) {
        ParseOptions options = new ParseOptions();
        options.setResolveFully(false);
        return new SwaggerConverter().readLocation(swaggerLocation, null, options).getOpenAPI();
    }


    public OpenAPI toOpenAPI() {
        return openAPI;
    }

    public void xmlSchema()
            throws IOException, ParserConfigurationException, TransformerException {

        final Config config =
                new Config.Builder().defRoot("components.schemas").build();

        //Parse global definitions.
        Document doc = new JsonSchema(Json.pretty(openAPI.getComponents().getSchemas()), config).toXMLSchema();
        //parse inlined definitions
        parseInlinedDefinitions(openAPI, doc);
        Assert.notNull(doc, "Schema Document should not be null");
    }

    private void parseInlinedDefinitions(OpenAPI api, Document doc) {
        Map<String, String> schemas = new HashMap<>();
        Paths paths = api.getPaths();
        for (Map.Entry<String, PathItem> path : paths.entrySet()) {
            OpenAPIPath oPath = new OpenAPIPath(path.getValue(), path.getKey());
            schemas.putAll(oPath.inlineRequestSchemas());
            schemas.putAll(oPath.inlineResponseSchemas());
        }
        for (Map.Entry<String, String> entry : schemas.entrySet()) {
            try {
                new JsonSchema(new ObjectMapper().readTree(entry.getValue()),
                        new Config.Builder().build(), doc)
                        .toXMLSchema(new ObjectMapper().readTree(entry.getValue()), entry.getKey());
            } catch (Exception e) {
                logger.severe(
                        "Failed creating schema for :" + entry.getKey() + ":" + Utils.getStackTrace(e));
            }
        }
    }
}
