package com.openapi.exceptions;

public class NotSupportedException extends Exception {

    public NotSupportedException(String s) {
        super(s);
    }

}
