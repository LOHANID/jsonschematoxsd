/* $Header: pcbpel/modules/adapter/cloud/designtime-framework/cloud.designtime.impl/src/main/java/oracle/tip/tools/ide/adapters/cloud/impl/metadata/rest/openapi/constants/JSONField.java dlohani_dlohani-openapiparser/4 2019/03/14 16:35:01 dlohani Exp $ */

/* Copyright (c) 2019, Oracle and/or its affiliates. All rights reserved.*/

/*
   DESCRIPTION
    <short description of component this file declares/defines>

   PRIVATE CLASSES
    <list of private classes defined - with one-line descriptions>

   NOTES
    <other useful comments, qualifications, etc.>

   MODIFIED    (MM/DD/YY)
    dlohani     01/22/19 - Creation
 */

/**
 *  @version $Header: pcbpel/modules/adapter/cloud/designtime-framework/cloud.designtime.impl/src/main/java/oracle/tip/tools/ide/adapters/cloud/impl/metadata/rest/openapi/constants/JSONField.java dlohani_dlohani-openapiparser/4 2019/03/14 16:35:01 dlohani Exp $
 *  @author  dlohani 
 *  @since   release specific (what release of product did this appear in)
 */

package com.openapi.constants;

public enum JSONField {
    name, properties, items, required,$ref,type,xml,attribute,namespace,prefix,format,nullable,description,additionalProperties;
}
