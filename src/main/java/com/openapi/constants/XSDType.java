package com.openapi.constants;

public enum XSDType {

    STRING("string"), INT("integer"), LONG("long"), BOOLEAN("boolean"), DATE("date"), TIME("time"), DATETIME(
            "dateTime"), DECIMAL("decimal"), URI("anyURI"), //Not real XSD types, placed here just for achieving parity with Json Types
    REFERENCE("ref"), ENUM("enum"), OBJECT("object"), ARRAY("xsdarray"), ANYOF("anyof");

    private final String name;

    XSDType(String s) {
        this.name = s;
    }

    public String value() {
        return name;
    }


}
