package com.openapi.constants;

public enum JSONType {
    string("string"),
    object("object"),
    array("array"),
    number("number"),
    bool("boolean"),
    integer("integer"),
    ref("$ref"),
    anyOf("anyOf"),
    oneOf("oneOf"),
    allOf("allOf"),
    not("not"),
    /*  schemas:
    AnyValue: {}*/
    any("any");


    private String name;

    JSONType(String s) {
        this.name = s;
    }

    @Override
    public String toString() {
        return name;
    }
}
