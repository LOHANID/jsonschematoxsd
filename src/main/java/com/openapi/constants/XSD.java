package com.openapi.constants;

public enum XSD {
    attribute, use, element, sequence, complexType, simpleType, restriction, value, choice, type, name, minOccurs,
    maxOccurs, optional, fixed, nillable, annotation, documentation,base, enumeration;
}