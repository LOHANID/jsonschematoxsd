package com.openapi.config;

public class Config {
    private final boolean allowInvalidNCNames;
    private String targetNamespace;
    private String nsAlias;
    private boolean createRootElement;
    private String name;
    private boolean attributesQualified;
    private boolean includeOnlyUsedTypes;
    private boolean validateXsdSchema;
    private boolean elementsQualified;
    private String defRoot;
    private String tnsAlias;

    public boolean isAttributesQualified() {
        return attributesQualified;
    }

    public boolean isElementQualified() {
        return elementsQualified;
    }


    public boolean isIncludeOnlyUsedTypes() {
        return includeOnlyUsedTypes;
    }

    public String getName() {
        return name;
    }

    public String targetNS() {
        return targetNamespace;
    }

    public String namespaceAlias() {
        return nsAlias;
    }

    public boolean isCreateRootElement() {
        return createRootElement;
    }

    public boolean isValidateXsdSchema() {
        return validateXsdSchema;
    }

    public String getDefRoot() {
        return defRoot;
    }

    public String targetNSAlias() {
        return tnsAlias;
    }

    public boolean isAllowInvalidNCNames() {
        return allowInvalidNCNames;
    }

    public static class Builder {
        private String defRoot= "";
        private String name;
        private String targetNamespace = "http://www.openapi.xmlschema.targetns";
        private String nsAlias = "xs:";
        private String tnsAlias = "tns:";

        private boolean createRootElement = false;
        private boolean attributesQualified = false;
        private boolean elementsQualified=true;
        private boolean includeOnlyUsedTypes = false;
        private boolean validateXsdSchema = true;
        private boolean allowInvalidNCNames = false;


        public Builder targetNamespace(String targetNamespace) {
            this.targetNamespace = targetNamespace;
            return this;
        }

        public Builder defRoot(String defRoot) {
            this.defRoot = defRoot;
            return this;
        }

        public Builder nsAlias(String nsAlias) {
            if (nsAlias==null) {
                this.nsAlias = "";
                return this;
            }
            this.nsAlias = nsAlias;
            return this;
        }

        public Builder tnsAlias(String tnsAlias) {
            if (tnsAlias==null) {
                this.tnsAlias = "";
                return this;
            }
            this.tnsAlias = nsAlias;
            return this;
        }

        public Builder createRootElement(boolean b) {
            this.createRootElement = b;
            return this;
        }

        public Builder includeOnlyUsedTypes(boolean b) {
            this.includeOnlyUsedTypes = b;
            return this;
        }

        public Config build() throws RuntimeException {
            if (targetNamespace == null) {
                throw new RuntimeException("Targetnamespace must not be null");
            }

            return new Config(this);
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder attributesQualified(boolean b) {
            this.attributesQualified = b;
            return this;
        }

        public Builder elementsQualified(boolean b) {
            this.elementsQualified = b;
            return this;
        }

        public Builder validateXsdSchema(boolean b) {
            this.validateXsdSchema = b;
            return this;
        }

        public void setAllowInvalidNCNames(boolean allowInvalidNCNames) {
            this.allowInvalidNCNames = allowInvalidNCNames;
        }
    }

    private Config(Builder builder) {
        this.targetNamespace = builder.targetNamespace;
        this.nsAlias = builder.nsAlias;
        this.createRootElement = builder.createRootElement;
        this.name = builder.name;
        this.attributesQualified = builder.attributesQualified;
        this.includeOnlyUsedTypes = builder.includeOnlyUsedTypes;
        this.validateXsdSchema = builder.validateXsdSchema;
        this.elementsQualified=builder.elementsQualified;
        this.defRoot=builder.defRoot;
        this.tnsAlias=builder.tnsAlias;
        this.allowInvalidNCNames = builder.allowInvalidNCNames;
    }
}
