package com.openapi.schema;

import com.fasterxml.jackson.databind.JsonNode;
import com.openapi.config.Config;
import com.openapi.constants.JSONField;
import com.openapi.constants.JSONType;
import com.openapi.constants.XSD;
import com.openapi.exceptions.NotSupportedException;
import com.openapi.util.TypeResolver;
import io.swagger.v3.core.util.Json;

import org.springframework.util.Assert;
import org.w3c.dom.Element;

public class ArrayElement extends SchemaElement {

    public ArrayElement(JsonNode node, String name, boolean required, Config config) {
        super(node, name, required, config);
    }

    /**
     * For a type definition like this
     * {
     * "errorDetails" : {
     * "minItems" : 1,
     * "type" : "array",
     * "items" : {
     * "required" : [ "errorCode", "errorPath", "instance", "title", "type" ],
     * "type" : "object",
     * "properties" : {
     * "instance" : {
     * "type" : "string"
     * },
     * "errorCode" : {
     * "type" : "string"
     * },
     * "type" : {
     * "type" : "string"
     * },
     * "title" : {
     * "type" : "string"
     * },
     * "errorPath" : {
     * "type" : "string"
     * }
     * }
     * }
     * }
     * }
     *
     * @param xmlElement :  The parent node where this node will be added
     * @return XMl Element corresponding to this Array type definition: example
     * <xs:element name="errorDetails" minOccurs="1" maxOccurs="unbounded" xmlns:xs="http://www.w3.org/2001/XMLSchema">
     * <xs:complexType>
     * <xs:sequence>
     * <xs:element name="instance" type="xs:string" minOccurs="1" maxOccurs="1"/>
     * <xs:element name="errorCode" type="xs:string" minOccurs="1" maxOccurs="1"/>
     * <xs:element name="type" type="xs:string" minOccurs="1" maxOccurs="1"/>
     * <xs:element name="title" type="xs:string" minOccurs="1" maxOccurs="1"/>
     * <xs:element name="errorPath" type="xs:string" minOccurs="1" maxOccurs="1"/>
     * </xs:sequence>
     * </xs:complexType>
     * </xs:element>
     */
    public Element parse(final Element xmlElement) throws Exception {
        doAsserts();

        Element element = null;
        switch (jsonType(node().get(JSONField.items.name()))) {
            case ref:
                element = createElement(xmlElement);
                element.setAttribute(XSD.type.name(), qualifiedType(node().get(JSONField.items.name())));
                return element;
            case object:
                element = createElement(xmlElement);
                //items node is an object type.Defined inline like this the object will be an anonymous type. Remove
                // type attribute
                element.removeAttribute(XSD.type.name());
                Element complexType = element(element, config().namespaceAlias() + XSD.complexType.name());
                return new JsonObjectType(node().get(JSONField.items.name()), config()).parse(complexType, name());
            case anyOf:
            case oneOf:
            case allOf:
            case not:
                throw new NotSupportedException(
                        "Mixed/Complex types not supported. Skipping Schema for : " + name() + ":" + Json
                                .pretty(node()));
            default:
                element = createElement(xmlElement);
                element.setAttribute(XSD.type.name(),
                                     config().namespaceAlias() + new TypeResolver(node().get(JSONField.items.name()))
                                             .type());
                return element;
        }
    }

    private Element createElement(Element xmlElement) throws Exception {
        Element element;
        element = element(xmlElement, name(XSD.element), false);
        super.parse(element);
        element.setAttribute(XSD.maxOccurs.name(), "unbounded");
        return element;
    }

    private void doAsserts() {
        Assert.isTrue(!node().get(JSONField.type.name()).isMissingNode(), "\"Type\" is missing from " + node());
        Assert.isTrue(!node().get(JSONField.items.name()).isMissingNode(),
                      "'items' must be present in array node:" + name());
        Assert.isTrue(JSONType.array.toString().equals(node().get(JSONField.type.name()).asText()),
                      "\"type\" must be array in " + node());
    }

    /**
     * @param type, example tns:components.schemas.Author
     * @return name from type , example Author
     */
    public String name(String type) {
        return type.replaceAll("(?:.*\\.)*", "");
    }
}