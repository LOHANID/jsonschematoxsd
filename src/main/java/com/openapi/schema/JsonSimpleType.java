package com.openapi.schema;

import com.fasterxml.jackson.databind.JsonNode;
import com.openapi.config.Config;
import org.w3c.dom.Element;

public class JsonSimpleType extends JsonType{
    public JsonSimpleType(JsonNode jsonTypeDefinition, Config config) {
        super(jsonTypeDefinition,config);
    }

    public Element parse(Element element, String name) throws Exception {
        return new SimpleElement(node(), elementName(name), true, config()).parse(element);
    }

}
