package com.openapi.schema;

import com.fasterxml.jackson.databind.JsonNode;
import com.openapi.config.Config;

import com.openapi.constants.JSONField;
import com.openapi.constants.XSD;
import org.springframework.util.Assert;
import org.w3c.dom.Element;

public class SimpleElement extends SchemaElement {


    /**
     * @param node     JsonNode that defines the element example: {"type":"string","xml":{"namespace":"http://www.books
     *                 .com/id","prefix":"nsi","attribute":true}}
     * @param name     element name
     * @param required if the node is required or not
     * @param config   com.openapi.dto.Context
     */
    public SimpleElement(JsonNode node, String name, boolean required, Config config) {
        super(node, name, required, config);
    }


    /**
     * @param xmlElement : element to parse this JSONNode to.
     * @return The new element created that represents this node.
     */
    public Element parse(final Element xmlElement) throws Exception {
        //Create an element node if this property is not an XML attribute.
        String ns = null;
        String prefix = null;

        if (isXmlNode(node())) {
            JsonNode nsNode = node().path(JSONField.xml.name()).path(JSONField.namespace.name());
            ns = nsNode.isMissingNode() ? null : nsNode.asText();
            JsonNode prefixNode = node().path(JSONField.xml.name()).path(JSONField.prefix.name());
            prefix = prefixNode.isMissingNode() ? "" : prefixNode.asText() + ":";
        }

        if (isXmlAttribute(node())) {
            Assert.isTrue(XSD.complexType.name().equals(xmlElement.getLocalName()), "element must be a complex type");
            //is attribute, create attribute node
            return attributeNode(xmlElement, node(), name(), required(), ns, prefix);
        }

        Element element = element(xmlElement, name(XSD.element), false);
        super.parse(element);

        return element;
    }


    private boolean isXmlAttribute(JsonNode simpleTypeDefinition) {
        JsonNode xmlNode = simpleTypeDefinition.path(JSONField.xml.name());
        return !xmlNode.isMissingNode() && !xmlNode.path(JSONField.attribute.name()).isMissingNode() && "true"
                .equals(xmlNode.path(JSONField.attribute.name()).asText());
    }

    private boolean isXmlNode(JsonNode simpleTypeDefinition) {
        return !simpleTypeDefinition.path(JSONField.xml.name()).isMissingNode();
    }

    private Element attributeNode(Element parent, JsonNode simpleTypeDefinition, String name, boolean required,
                                  String ns, String prefix) {
        Element attrib = element(parent, config().namespaceAlias() + XSD.attribute.name());
        attrib.setAttribute(XSD.name.name(), name);
        if (!required) attrib.setAttribute(XSD.use.name(), XSD.optional.name());
        attrib.setAttribute(XSD.type.name(), config().namespaceAlias() + xsdType(simpleTypeDefinition).value());
        return attrib;
    }
}
