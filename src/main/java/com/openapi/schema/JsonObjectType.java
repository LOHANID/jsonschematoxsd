package com.openapi.schema;

import com.fasterxml.jackson.databind.JsonNode;
import com.openapi.config.Config;

import com.openapi.constants.JSONField;
import com.openapi.constants.JSONType;
import com.openapi.constants.XSD;
import com.openapi.exceptions.NotSupportedException;
import com.openapi.util.TypeResolver;
import org.springframework.util.Assert;
import org.w3c.dom.Element;

import java.util.Iterator;
import java.util.Map;

public class JsonObjectType extends JsonType {

    public JsonObjectType(JsonNode jsonNode, Config config) {
        super(jsonNode, config);
    }

    /**
     * Sample node:
     * {
     * "required" : [ "ISBN", "author", "id", "name", "price", "publisher", "publisherinlined" ],
     * "type" : "object",
     * "properties" : {
     * "id" : {
     * "type" : "string"
     * },
     * "name" : {
     * "type" : "string"
     * },
     * "ISBN" : {
     * "type" : "string"
     * },
     * "price" : {
     * "type" : "integer"
     * },
     * "author" : {
     * "type" : "array",
     * "items" : {
     * "$ref" : "#/components/schemas/Author"
     * }
     * },
     * "publisher" : {
     * "$ref" : "#/components/schemas/Publisher"
     * },
     * "publisherinlined" : {
     * "required" : [ "id", "name" ],
     * "type" : "object",
     * "properties" : {
     * "name" : {
     * "type" : "string"
     * },
     * "id" : {
     * "type" : "string"
     * },
     * "childPublisher" : {
     * "$ref" : "#/components/schemas/Publisher"
     * }
     * }
     * }
     * }
     * }
     * This method populates the given complex type using the provided JsonNode. The complexType may or may not have
     * a name attribute.
     *
     * @param complexType : A anonymous or named complex type to add the elements to. example <xs:complexType
     *                    name="components.schemas.Book" xmlns:xs="http://www.w3.org/2001/XMLSchema"/>
     * @return Completed complex type; Example
     * <xs:complexType name="components.schemas.Book" xmlns:xs="http://www.w3.org/2001/XMLSchema">
     * <xs:sequence>
     * <xs:element name="id" type="xs:string" minOccurs="1" maxOccurs="1"/>
     * <xs:element name="name" type="xs:string" minOccurs="1" maxOccurs="1"/>
     * <xs:element name="ISBN" type="xs:string" minOccurs="1" maxOccurs="1"/>
     * <xs:element name="price" type="xs:int" minOccurs="1" maxOccurs="1"/>
     * <xs:element name="author" type="tns:components.schemas.Author" minOccurs="1" maxOccurs="unbounded"/>
     * <xs:element name="publisher" type="tns:components.schemas.Publisher" minOccurs="1" maxOccurs="1"/>
     * <xs:element name="publisherinlined" minOccurs="1" maxOccurs="1">
     * <xs:complexType>
     * <xs:sequence>
     * <xs:element name="name" type="xs:string" minOccurs="1" maxOccurs="1"/>
     * <xs:element name="id" type="xs:string" minOccurs="1" maxOccurs="1"/>
     * <xs:element name="childPublisher" type="tns:components.schemas.Publisher" minOccurs="0" maxOccurs="1"/>
     * </xs:sequence>
     * </xs:complexType>
     * </xs:element>
     * </xs:sequence>
     * </xs:complexType>
     */
    public Element parse(Element complexType, String name) throws Exception {
        Assert.isTrue(XSD.complexType.name().equals(complexType.getLocalName()), "element must be a complex type");
        if (node().get(JSONField.properties.name()) != null) {
            parseProperties(complexType);
        }
        if (node().get(JSONField.additionalProperties.name()) != null){

        }
        return complexType;

    }

    private void parseProperties(Element complexType) throws Exception {
        Element schemaSequence = null;
        final Iterator<Map.Entry<String, JsonNode>> allProperties = node().get(JSONField.properties.name()).fields();
        while (allProperties.hasNext()) {
            try {
                final Map.Entry<String, JsonNode> entry = allProperties.next();
                final String key = entry.getKey();
                final JsonNode val = entry.getValue();
                //Create a sequence element if this property is not an attribute. Create the sequence element only once
                if (schemaSequence == null && !isXmlAttrib(val))
                    schemaSequence = element(complexType, config().namespaceAlias() + XSD.sequence.name(), true);

                final JSONType jsonType = new TypeResolver(val).jsonType();

                switch (jsonType) {
                    case integer:
                    case string:
                    case number:
                    case bool:
                        new SimpleElement(val, key, required(key), config())
                                .parse(schemaSequence == null ? complexType : schemaSequence);
                        break;
                    case ref:
                        new ReferenceElement(val, key, required(key), config()).parse(schemaSequence);
                        break;
                    case array:
                        new ArrayElement(val, key, required(key), config()).parse(schemaSequence);
                        break;
                    case object:
                        new ObjectElement(val, key, required(key), config()).parse(schemaSequence);
                        break;
                    case anyOf:
                    case allOf:
                    case oneOf:
                        new MixedElement(val, key, required(key), config()).parse(schemaSequence);
                        break;
                    default:
                        new AnyTypeElement(val, key, required(key), config()).parse(schemaSequence);
                        break;

                }
            } catch (NotSupportedException nse) {
                logger().warning(nse.getMessage());
            }
        }
    }

    private boolean isXmlAttrib(JsonNode val) {
        JsonNode xmlNode = val.path(JSONField.xml.name());
        return !xmlNode.isMissingNode() && !xmlNode.path(JSONField.attribute.name()).isMissingNode() && "true"
                .equals(xmlNode.path(JSONField.attribute.name()).asText());
    }

}
