package com.openapi.schema;

import com.fasterxml.jackson.databind.JsonNode;
import com.openapi.config.Config;
import com.openapi.constants.JSONField;
import com.openapi.constants.XSD;
import com.openapi.constants.XSDType;
import com.openapi.exceptions.NotSupportedException;
import io.swagger.v3.core.util.Json;

import org.springframework.util.Assert;
import org.w3c.dom.Element;

public class JsonArrayType extends JsonType {

    public JsonArrayType(JsonNode jsonArray, Config config) {
        super(jsonArray, config);
    }

    public Element parse(Element complexType, String name) throws Exception {
        final JsonNode arrItems = node().path(JSONField.items.name());
        Assert.notNull(arrItems, "Array type must have an \"items\" node");

        Element elem = null;
        switch (jsonType(arrItems)) {
            case object:
                new JsonObjectType(node().get(JSONField.items.name()), config()).parse(complexType, name);
                break;
            case allOf:
            case anyOf:
            case oneOf:
            case not:
                throw new NotSupportedException(
                        "Mixed/Complex types not supported. Skipping Schema:" + Json.pretty(node()));
            case ref:
                elem = createElementSequence(complexType);
                elem.setAttribute(XSDType.REFERENCE.value(), config().targetNSAlias() + type(arrItems));
                return elem;
            case array:
                new JsonArrayType(arrItems, config()).parse(complexType, name);
            case bool:
            case number:
            case string:
            case integer:
                elem = createElementSequence(complexType);
                elem.setAttribute(XSD.type.name(), config().namespaceAlias() + xsdType(arrItems).value());
                return elem;
            case any:
                throw new NotSupportedException("'any' type not supported. Skipping Schema::" + Json.pretty(node()));

        }
        return complexType;
    }

    private Element createElementSequence(Element complexType) {
        Element schemaSequence = element(complexType, config().namespaceAlias() + XSD.sequence.name(), true);
        Element elem = element(schemaSequence, name(XSD.element, config()), false);
        elem.setAttribute(XSD.minOccurs.name(), "0");
        elem.setAttribute(XSD.maxOccurs.name(), "unbounded");
        return elem;
    }


}
