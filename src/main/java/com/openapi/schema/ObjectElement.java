package com.openapi.schema;

import com.fasterxml.jackson.databind.JsonNode;
import com.openapi.config.Config;
import com.openapi.constants.JSONField;
import com.openapi.constants.JSONType;
import com.openapi.constants.XSD;
import org.springframework.util.Assert;
import org.w3c.dom.Element;

public class ObjectElement extends SchemaElement {

    public ObjectElement(JsonNode node, String name, boolean required, Config config) {
        super(node, name, required, config);
    }

    /**
     * {
     * "publisherinlined" : {
     * "type" : "object",
     * "properties" : {
     * "name" : {
     * "type" : "string"
     * },
     * "id" : {
     * "type" : "string"
     * },     *
     * "childPublisher" : {
     * "$ref" : "#/components/schemas/Publisher"
     * }
     * },
     * "required":["name","id"]
     * }
     * }
     * <p>
     * This method returns a XML element node corresponding to the provided Json Type Definition.
     *
     * @param xmlElement : Parent node
     * @return element corresponding to provided Json tyoe definition
     * <xs:element name="publisherinlined" minOccurs="1" maxOccurs="1" xmlns:xs="http://www.w3.org/2001/XMLSchema">
     * <xs:complexType>
     * <xs:sequence>
     * <xs:element name="name" type="xs:string" minOccurs="1" maxOccurs="1"/>
     * <xs:element name="id" type="xs:string" minOccurs="1" maxOccurs="1"/>
     * <xs:element name="childPublisher" type="tns:components.schemas.Publisher" minOccurs="0" maxOccurs="1"/>
     * </xs:sequence>
     * </xs:complexType>
     * </xs:element>
     */
    public Element parse(final Element xmlElement) throws Exception {
        Assert.isTrue(JSONType.object.toString().equals(node().get(JSONField.type.name()).asText()),
                "\"type\" node is not \"object\" or is missing in :" + node());
        Element element = element(xmlElement, name(XSD.element), false);
        super.parse(element);
        element.setAttribute(XSD.maxOccurs.name(), "1");
        //This will be an anonymous type, so no type attribute.
        element.removeAttribute(XSD.type.name());
        //Create a complex type child element
        Element complexType = element(element, config().namespaceAlias() + XSD.complexType.name());
        new JsonObjectType(node(), config()).parse(complexType, name());
        return element;
    }
}