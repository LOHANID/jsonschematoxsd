package com.openapi.schema;

import com.fasterxml.jackson.databind.JsonNode;
import com.openapi.config.Config;

import com.openapi.constants.JSONField;
import com.openapi.constants.JSONType;
import com.openapi.constants.XSD;
import com.openapi.constants.XSDType;
import com.openapi.util.Utils;
import com.openapi.util.TypeResolver;
import com.openapi.util.XMLHelper;
import org.springframework.util.Assert;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.HashSet;
import java.util.Iterator;
import java.util.logging.Logger;

public abstract class JsonType {
    private final JsonNode node;
    private final Config config;
    private Logger logger = com.openapi.util.Logger.get();

    public HashSet<String> requiredProperties() {
        return requiredProperties;
    }

    private HashSet<String> requiredProperties;

    public JsonType(JsonNode jsonNode, Config config) {
        this.node = jsonNode;
        this.config = config;
        this.requiredProperties = getRequiredProperties();
    }

    public JsonNode node() {
        return this.node;
    }

    public Config config() {
        return this.config;
    }

    public Logger logger() {
        return logger;
    }

    /*This method creates a child node with the given name under the given parent*/
    public Element element(Node parent, String name, boolean createAsFirstChild) {
        return new XMLHelper(parent).createElement(name, createAsFirstChild);
    }

    public String name(XSD xsdType, Config config) {
        if (config.namespaceAlias() != null) return config.namespaceAlias() + xsdType.name();
        return xsdType.name();
    }

    private HashSet<String> getRequiredProperties() {
        if (node.path(JSONField.required.name()).isMissingNode()) return new HashSet<String>();
        Assert.isTrue(node.path(JSONField.required.name()).isArray(), "'required' property must have type: array");
        final Iterator<JsonNode> requiredProperties = node.get(JSONField.required.name()).elements();
        HashSet<String> requiredFields = new HashSet<String>();
        while (requiredProperties.hasNext()) {
            JsonNode property = requiredProperties.next();
            Assert.isTrue(property.isTextual(), "required must be string");
            requiredFields.add(property.asText());
        }
        return requiredFields;
    }
    public abstract Element parse(Element complexType, String name) throws Exception;

    public XSDType xsdType(JsonNode node) {
        return new TypeResolver(node).xsdType();
    }

    public String type(JsonNode node) {
        return new TypeResolver(node).type();
    }

    public JSONType jsonType(JsonNode typeDef) {
        return new TypeResolver(typeDef).jsonType();
    }

    public boolean required(String key) {
        return requiredProperties.contains(key);
    }
    public String elementName(String name) {
        name = config.isAllowInvalidNCNames()?name: Utils.normalize(name);
        if (config().getDefRoot() != null) return config().getDefRoot() + "." + name;
        return name;
    }

    public Element complexType(String name, Element parent) {
        Element complexType;
        complexType = element(parent, config.namespaceAlias() + XSD.complexType.name(),false);
        complexType.setAttribute(XSD.name.name(), elementName(name));
        return complexType;
    }
}
