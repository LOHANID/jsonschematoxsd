package com.openapi.schema;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.openapi.config.Config;
import com.openapi.constants.XSD;
import com.openapi.util.TypeMappings;
import com.openapi.util.XMLHelper;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.logging.Logger;

public class Restriction {
    private final Config config;
    private final Element parent;
    private final String base;
    private Element restrictionElem;
    private JsonNode enums;
    private final Logger logger = com.openapi.util.Logger.get();
    public Restriction(Element parent, String base, Config config) {
        this.config = config;
        this.parent = parent;
        this.base = base;
    }

    private Element getOrCreateRestrictionElement(Element parent, String base, Config config) {
        if (restrictionElem == null) {
            Element element = element(parent, name(XSD.simpleType), false);
            element = element(element, name(XSD.restriction), false);
            element.setAttribute(XSD.base.name(), config.namespaceAlias() + base);
            restrictionElem=element;
        }
        return restrictionElem;
    }

    public Restriction enumerations(JsonNode enums) {
        this.enums = enums;
        return this;
    }

    public void apply() {
        switch(TypeMappings.instance().get(base)){
            case INT:
            case DECIMAL:
            case DATE:
            case DATETIME:
            case LONG:
            case TIME:
            case URI:
            case STRING:
                doEnums();
                return;
            default:
                logger.info("No restriction applied for:" + parent.getAttribute(XSD.name.name()));
        }



    }

    private void doEnums() {
        if (enums != null) {
            ArrayNode array = (ArrayNode) enums;
            for (int i = 0; i < array.size(); i++) {
                Element element = element(getOrCreateRestrictionElement(parent, base, config), name(XSD.enumeration), false);
                element.setAttribute("value", array.get(i).asText());
            }
        }
    }

    private String name(XSD xsdType) {
        if (config.namespaceAlias() != null) return config.namespaceAlias() + xsdType.name();
        return xsdType.name();
    }

    public Element element(Node parent, String name, boolean createAsFirstChild) {
        return new XMLHelper(parent).createElement(name, createAsFirstChild);
    }
}
