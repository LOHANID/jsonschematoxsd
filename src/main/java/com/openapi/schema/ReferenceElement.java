package com.openapi.schema;

import com.fasterxml.jackson.databind.JsonNode;
import com.openapi.config.Config;

import com.openapi.constants.JSONType;
import com.openapi.constants.XSD;
import org.springframework.util.Assert;
import org.w3c.dom.Element;

public class ReferenceElement extends SchemaElement {

    public ReferenceElement(JsonNode node, String name, boolean required, Config config) {
        super(node,name,required,config);
    }

    public Element parse(final Element xmlElement) throws Exception {
        Assert.isTrue(!node().get(JSONType.ref.toString()).isMissingNode(),
                      "'missing $ref for " + name());
        Element elem = element(xmlElement, name(XSD.element), false);
        super.parse(elem);
        elem.setAttribute(XSD.type.name(), qualifiedType(node()));
        return elem;
    }
}
