package com.openapi.schema;

import com.fasterxml.jackson.databind.JsonNode;
import com.openapi.config.Config;
import com.openapi.exceptions.NotSupportedException;
import io.swagger.v3.core.util.Json;
import org.w3c.dom.Element;

public class AnyTypeElement extends SchemaElement{
    public AnyTypeElement(JsonNode node, String name, boolean required, Config config) {
        super(node,name,required,config);
    }

    public Element parse(Element nodeElem) throws Exception{
        throw new NotSupportedException("'Any'type is not supported: Skipping:" + name()+":"+ Json
                .pretty(node()));

    }
}
