package com.openapi.schema;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.openapi.config.Config;
import com.openapi.constants.JSONType;
import com.openapi.constants.XSD;
import com.openapi.exceptions.NotSupportedException;
import com.openapi.util.Utils;
import com.openapi.util.TypeResolver;
import com.openapi.util.XMLHelper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

public class JsonSchema {

    private final JsonNode definitions;
    private final Document schemaDoc;
    private final Config config;
    private Logger logger = com.openapi.util.Logger.get();


    public JsonSchema(JsonNode definitions, Config config) throws ParserConfigurationException {
        this.definitions = definitions;
        this.config = config;
        this.schemaDoc = createDocument();
    }

    public JsonSchema(JsonNode definitions, Config config, Document document) {
        this.definitions = definitions;
        this.config = config;
        this.schemaDoc = document;
    }

    public JsonSchema(String definition, Config config)
            throws IOException, ParserConfigurationException {
        this(new ObjectMapper().readTree(definition), config);
    }

    public Document toXMLSchema() {
        for (Iterator<Map.Entry<String, JsonNode>> it = definitions.fields(); it.hasNext(); ) {
            Map.Entry<String, JsonNode> schema = it.next();
            try {
                toXMLSchema(schema.getValue(),
                        config.isAllowInvalidNCNames() ? schema.getKey() : Utils
                                .normalize(schema.getKey()));
            } catch (Exception e) {
                logger.severe(e.getMessage()+":"+ Utils.getStackTrace(e));
            }
        }
        return schemaDoc;
    }


    /**
     * For a Json schema like the following
     * {
     * "getEmployee-response-wrapper" : {
     * "required" : [ "division", "id", "name" ],
     * "type" : "object",
     * "properties" : {
     * "id" : {
     * "type" : "string"
     * },
     * "name" : {
     * "type" : "string"
     * },
     * "division" : {
     * "type" : "string"
     * }
     * }
     * }
     * }
     * This is an optimistic parse. It assumes that for each jsonTypedefinition provided, the definitions of types it
     * is referring to
     * will also be provided at some later point in time. This implies that while creating types it will not actively
     * look for the
     * types being referred to in the current type with the assumption that by the time the schema document is
     * completely written
     * it will have types corresponding to all the types defined in the JSON schema.
     *
     * @param jsonTypeDefinition: json Type definition for example {"required":["division","id","name"],
     *                            "type":"object","properties":{"id":{"type":"string"},"name":{"type":"string"},
     *                            "division":{"type":"string"}}}
     * @param name:               Name given to the type definition, example getEmployee-response-wrapper
     * @return
     */
    public Document toXMLSchema(final JsonNode jsonTypeDefinition, final String name) throws Exception {
        //create complex type node
        Element documentElement = schemaDoc.getDocumentElement();

        try {
            Element element = createType(jsonTypeDefinition, name, documentElement);

            //The type has been created, now create the element. Skip creating an element if one is already created
            // for example for definitions like "updateOrder_response": {
            //
            //    "$ref": "#/definitions/updateOrder_request"
            //
            //}
            if (!element.getNodeName().equals(config.namespaceAlias() + XSD.element.name())) createElement(name);

        } catch (NotSupportedException e) {
            logger.severe(Utils.getStackTrace(e));
        }
        return schemaDoc;

    }

    private Element createElement(String name) {
        final Element rootElement =
                element(schemaDoc.getDocumentElement(), config.namespaceAlias() + XSD.element.name());
        rootElement.setAttribute(XSD.name.name(),
                name(name));
        rootElement.setAttribute(XSD.type.name(),
                config.targetNSAlias() != null ? config.targetNSAlias() + name(name) : name(name));
        return rootElement;
    }

    private Element createType(JsonNode jsonTypeDefinition, String name, Element parent) throws Exception {
        JSONType type = new TypeResolver(jsonTypeDefinition).jsonType();
        switch (type) {
            case array:
                return new JsonArrayType(jsonTypeDefinition, config).parse(createComplexType(name, parent), name);
            case anyOf:
            case oneOf:
            case allOf:
            case not:
                return new JsonMixedType(jsonTypeDefinition, config).parse(null, name);
            case object:
                return new JsonObjectType(jsonTypeDefinition, config).parse(createComplexType(name, parent), name);
            case bool:
            case number:
            case string:
            case integer:
                return new JsonSimpleType(jsonTypeDefinition, config).parse(parent, name);
            case ref:
                return new JsonRefType(jsonTypeDefinition, config).parse(parent, name);
            case any:
                return new JsonAnyType(jsonTypeDefinition, config).parse(createComplexType(name, parent), name);

        }
        return parent;
    }

    private Element createComplexType(String name, Element parent) {
        Element complexType;
        complexType = element(parent, config.namespaceAlias() + XSD.complexType.name());
        complexType.setAttribute(XSD.name.name(), name(name));
        return complexType;
    }

    private String name(String name) {
        name = config.isAllowInvalidNCNames() ? name : Utils.normalize(name);
        if (!Utils.isNull(config.getDefRoot()))
            return config.getDefRoot() + "." + name;
        return name;
    }

    /*This method creates a child node with the given name under the given parent*/
    private Element element(Node parent, String name) {
        return new XMLHelper(parent).createElement(name);
    }

    private Document createDocument() throws ParserConfigurationException {
        final Document xsdDoc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        xsdDoc.setXmlStandalone(true);
        final Element schemaRoot = element(xsdDoc, config.namespaceAlias() + "schema");
        schemaRoot.setAttribute("targetNamespace", config.targetNS());
        schemaRoot.setAttribute("xmlns:" + config.targetNSAlias().substring(0, config.targetNSAlias().length() - 1),
                config.targetNS());
        schemaRoot.setAttribute("elementFormDefault", "qualified");
        if (config.isAttributesQualified()) {
            schemaRoot.setAttribute("attributeFormDefault", "qualified");
        }
        if (config.isElementQualified()) {
            schemaRoot.setAttribute("elementFormDefault", "qualified");
        }
        return xsdDoc;
    }
}
