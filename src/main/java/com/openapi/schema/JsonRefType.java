package com.openapi.schema;

import com.fasterxml.jackson.databind.JsonNode;
import com.openapi.config.Config;
import org.w3c.dom.Element;

public class JsonRefType extends JsonType {
    public JsonRefType(JsonNode jsonTypeDefinition, Config config) {
        super(jsonTypeDefinition, config);

    }

    public Element parse(Element schemaRoot, String name) throws Exception {
        return new ReferenceElement(node(), elementName(name), true, config()).parse(schemaRoot);
    }
}
