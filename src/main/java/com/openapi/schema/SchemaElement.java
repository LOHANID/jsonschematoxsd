package com.openapi.schema;

import com.fasterxml.jackson.databind.JsonNode;
import com.openapi.config.Config;
import com.openapi.constants.JSONField;
import com.openapi.constants.JSONType;
import com.openapi.constants.XSD;
import com.openapi.constants.XSDType;
import com.openapi.util.Utils;
import com.openapi.util.TypeResolver;
import com.openapi.util.XMLHelper;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.logging.Logger;

public class SchemaElement {

    private final Config config;
    private final JsonNode node;
    private final String name;
    private final boolean required;
    private Logger logger = com.openapi.util.Logger.get();

    public SchemaElement(JsonNode node, String name, boolean required, Config config) {
        this.node = node;
        this.name = config.isAllowInvalidNCNames() ? name : Utils.normalize(name);
        this.required = required;
        this.config = config;
    }

    public Logger logger() {
        return logger;
    }

    public Config config() {
        return this.config;
    }

    public JsonNode node() {
        return this.node;
    }

    public String name() {
        return this.name;
    }

    public boolean required() {
        return required;
    }


    public Element parse(final Element xmlElement) throws Exception {
        setName(xmlElement);
        setRequired(xmlElement);
        setNillable(xmlElement);
        addAnnotations(xmlElement);
        addRestrictions(xmlElement);

        return xmlElement;
    }

    private void addRestrictions(Element parent) {
        Restriction restrictions = new Restriction(parent, new TypeResolver(node()).xsdType().value(), config());
        if (isEnum(node())) {
            restrictions.enumerations(node().get("enum")).apply();
            return;
        }
        setType(parent);
    }

    /*{"maxLength":5000,"type":"string","description":"A CSS hex color value representing the primary branding color
    for this account","nullable":true}*/
    private void addAnnotations(Element parent) {
        if (node().get(JSONField.description.name()) == null) return;
        Element annotation = element(parent, config.namespaceAlias() + XSD.annotation.name(),true);
        Element documentation = element(annotation, config.namespaceAlias() + XSD.documentation.name());
        documentation.setTextContent(node().get(JSONField.description.name()).asText());
    }

    private void setNillable(Element parent) {
        if (node().path(JSONField.nullable.name()).isMissingNode()) return;
        parent.setAttribute(XSD.nillable.name(),
                Boolean.toString(node().path(JSONField.nullable.name()).booleanValue()));
    }

    private void setRequired(Element parent) {
        if (!required) parent.setAttribute(XSD.minOccurs.name(), "0");
    }

    private void setType(Element parent) {
        parent.setAttribute(XSD.type.name(), config.namespaceAlias() + new TypeResolver(node()).xsdType().value());
    }

    private void setName(Element parent) {
        parent.setAttribute(XSD.name.name(), name);
    }

    public String qualifiedType(JsonNode typeDef) {
        if (config.targetNSAlias() != null) return config.targetNSAlias() + new TypeResolver(typeDef).type();
        return new TypeResolver(typeDef).type();
    }

    /*This method creates a child node with the given name under the given parent*/
    public Element element(Node parent, String name) {
        return new XMLHelper(parent).createElement(name);
    }

    public XSDType xsdType(JsonNode simpleTypeDefinition) {
        return new TypeResolver(simpleTypeDefinition).xsdType();
    }

    public String name(String name) {
        if (config.getDefRoot() != null) return config.getDefRoot() + "." + name;
        return name;
    }

    public JSONType jsonType(JsonNode typeDef) {
        return new TypeResolver(typeDef).jsonType();
    }

    public String name(XSD xsdType) {
        if (config.namespaceAlias() != null) return config.namespaceAlias() + xsdType.name();
        return xsdType.name();
    }

    /*This method creates a child node with the given name under the given parent*/
    public Element element(Node parent, String name, boolean createAsFirstChild) {
        return new XMLHelper(parent).createElement(name, createAsFirstChild);
    }

    public boolean isEnum(JsonNode node) {
        return !(node.get("enum")==null);
    }

}


