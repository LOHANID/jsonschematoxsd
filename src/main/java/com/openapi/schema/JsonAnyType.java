package com.openapi.schema;

import com.fasterxml.jackson.databind.JsonNode;
import com.openapi.config.Config;
import com.openapi.exceptions.NotSupportedException;
import io.swagger.v3.core.util.Json;
import org.w3c.dom.Element;

public class JsonAnyType extends JsonType{
    public JsonAnyType(JsonNode jsonTypeDefinition, Config config) {
        super(jsonTypeDefinition,config);

    }

    public Element parse(Element complexType, String name) throws Exception{
        throw new NotSupportedException(
                "'Any' Type are not supported yet. Skipping: " + name +":"+ Json.pretty(node()));
    }
}
