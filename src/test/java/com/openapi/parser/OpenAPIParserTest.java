package com.openapi.parser;

import org.junit.Test;

import static org.junit.Assert.*;

public class OpenAPIParserTest {
    @Test
    public void testStripeOpenAPI() {
        OpenAPIParser parser = new OpenAPIParser("src/test/resources/stripeopenapi.json",false);
        try {
            parser.xmlSchema();
        } catch (Exception e) {
            e.printStackTrace();
            fail("Failed:" + e.getMessage());
        }
    }
}